require('./api/startup');
const express = require('express');
const cors = require('cors');
const bodyparser = require('body-parser');
const app = express();
const portNumber = 3000;
const sourceDir = 'dist';
const queues = require('./api/queue');

app.use(function(req, res, next) {
  res.header('Cache-Control', 'private, no-cache, no-store, must-revalidate');
  res.header('Expires', '-1');
  res.header('Pragma', 'no-cache');
  next();
});
app.use(bodyparser.json({}));
app.use(cors());

app.use(express.static(sourceDir));
app.use('/queues', queues);

app.listen(portNumber, () => {
  console.log(`Express web server started: http://localhost:${portNumber}`);
  console.log(`Serving content from /${sourceDir}/`);
});
