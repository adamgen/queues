const db = require('./db');
const queueLog = require('./log').queueLog;
const _ = require('lodash');

const list = db.get();
_.orderBy(queueLog, data => data.priority);
list.forEach(queueLog);
