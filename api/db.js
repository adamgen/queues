const fs = require('fs');
const _ = require('lodash');

const path = __dirname + '/db/file.txt';

function guid() {
  const s4 = () => Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1);
  return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
}

const get = () => {
  const contentString = fs.readFileSync(path).toString();
  const fileContent = contentString ? JSON.parse(contentString) : [];
  return fileContent;
};

const set = (content) => {
  const fileContent = get();
  content.id = guid();
  fileContent.push(content);
  fs.writeFileSync(path, JSON.stringify(fileContent));
  return content.id;
};

const remove = id => {
  const fileContent = get();
  _.remove(fileContent, data => data.id === id);
  fs.writeFileSync(path, JSON.stringify(fileContent));
};

module.exports = {
  set,
  get,
  remove,
};