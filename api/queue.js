require('colors');
const router = require('express').Router();
const _ = require('lodash');
const db = require('./db');
const queueLog = require('./log').queueLog;

router.get('/', (req, res) => {
  res.status(200).json({status: 'success', list: db.get()});
});

router.post('/', (req, res) => {
  const data = _.pickBy(req.body, (value, key) => ['payload', 'time', 'priority'].includes(key));
  queueLog(data);

  res.status(200).json({status: 'success'});
});

router.delete('/', (req, res) => {
  res.status(200).json({status: 'success', list: db.remove(req.body.id)});
});

module.exports = router;
