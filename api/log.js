const _ = require('lodash');
const db = require('./db');

const queueTimeLists = {};

const log = data => {
  const now = new Date(Date.now() + (1000 * 60 * 60 * 3)).toISOString().match(/\d{4}-\d{2}-\d{2}T\d{2}:\d{2}/)[0];
  console.log(`[${now}]`.blue + `[${data.time}]`.red + `[${data.payload}]`.green);
};

const logAndRemove = (data) => {
  log(data);
  if(data.id) db.remove(data.id);
};

const initQueueList = data => {
  queueTimeLists[data.time] = [];
  return new Date(data.time) - new Date;
};

const pushToQueueList = data => {
  queueTimeLists[data.time].push(data);
  data.id = db.set(data);
};

const queueLog = (data) => {
  if(!data.time || new Date(data.time) <= new Date()) {
    return logAndRemove(data);
  } // else

  if(queueTimeLists[data.time]) {
    pushToQueueList(data);
    return;
  } // else

  const remainingTime = initQueueList(data);
  pushToQueueList(data);
  setTimeout(() => {
    _.each(_.orderBy(queueTimeLists[data.time], data => data.priority), logAndRemove);
  }, remainingTime);
};

module.exports = {
  log,
  queueLog,
};
