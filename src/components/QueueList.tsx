import * as React from 'react';
import axios from 'axios';
import * as _ from 'lodash';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Table from '@material-ui/core/Table';

export default class QueueList extends React.Component {
  state = {
    list: [],
  };

  constructor(props) {
    super(props);

    this.getData();
  }

  getData = async () => {
    const data = await axios.get('http://localhost:3000/queues');
    const list = _.get(data, 'data.list', []);
    this.setState({ list }, () => {
      console.log(list);
    });
  };

  render() {
    return (
      <div style={{ 'margin': '20px' }}>
        <Table>
          <TableHead>
            <TableRow>
              <TableCell>payload</TableCell>
              <TableCell>time</TableCell>
              <TableCell>priority</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {this.state.list.map((item) => {
              console.log(item);
              return (
                <TableRow key={item.id}>
                  <TableCell>{item.payload}</TableCell>
                  <TableCell>{item.time}</TableCell>
                  <TableCell>{item.priority}</TableCell>
                </TableRow>
              );
            })}
          </TableBody>
        </Table>
      </div>
    );
  }
}