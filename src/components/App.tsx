import * as React from 'react';

import AppHeader from './AppHeader';
import InsertQueueForm from './InsertQueueForm';
import QueueList from './QueueList';

// import "./../assets/scss/App.scss";


export interface AppProps {
}

export default class App extends React.Component<AppProps, undefined> {
  constructor(props) {
    super(props);

    this.handleChange = this.handleChange.bind(this);
    this.submit = this.submit.bind(this);
  }

  handleChange(event) {
    this.setState({ value: event.target.value });
  }

  submit() {
    console.log(this.state);
  }

  render() {
    return (
      <div className="app">
        <AppHeader></AppHeader>
        <br/>
        <InsertQueueForm></InsertQueueForm>
        <QueueList/>
      </div>
    );
  }
}
