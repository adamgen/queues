import * as React from 'react';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import axios from 'axios';
import * as chance from 'chance';

interface FormState {
  payload: string;
  time: string;
  priority: string;
}

let payloadCounter = 0;
const time = () => new Date(Date.now() + 1000 * 60 + (1000 * 60 * 60 * 3)).toISOString().match(/\d{4}-\d{2}-\d{2}T\d{2}:\d{2}/)[0];
let priority;
const resetState = () => ({
  priority: priority = chance().integer({ min: -20, max: 20 }),
  payload: `${payloadCounter++} - ${chance().word()} [${priority}]`,
  time: time(),
});

export default class App extends React.Component {
  state: FormState = resetState();

  constructor(props) {
    super(props);

    this.handleChange = this.handleChange.bind(this);
    this.submit = this.submit.bind(this);
  }

  handleChange(key) {
    return (event) => {
      const obj = {};
      obj[key] = event.target.value;
      this.setState(obj);
    };
  }

  reset() {
    this.setState({ ...resetState(), time: this.state.time });
  }

  submit() {
    axios.post('http://localhost:3000/queues', this.state);
    this.reset();
  }

  render() {
    return (
      <Grid container
            alignItems="flex-end"
            justify="center"
            spacing={16}>
        <Grid item>
          <TextField
            value={this.state.payload}
            onChange={this.handleChange('payload')}
            id="standard-name"
            label="Payload"

          />
        </Grid>
        <Grid item>
          <TextField
            id="datetime-local"
            label="Execution time"
            type="datetime-local"
            // defaultValue="2017-05-24T10:30"
            // defaultValue={new Date()}
            value={this.state.time}
            onChange={this.handleChange('time')}
            InputLabelProps={{
              shrink: true,
            }}
          />
        </Grid>
        <Grid item>
          <TextField
            value={this.state.priority}
            onChange={this.handleChange('priority')}
            id="standard-name"
            label="Priority"
            type="number"
          />
        </Grid>
        <Grid item>
          <Button color="primary" onClick={this.submit}>Submit</Button>
        </Grid>
      </Grid>
    );
  }
}